package work.metanet.demo.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import work.metanet.demo.model.Device;
import work.metanet.demo.repositories.DeviceRepository;

@Controller
public class DeviceController {
    @Autowired
    private DeviceRepository deviceRepository;

    public DeviceController() {
        super();
    }

    @GetMapping({"/", "/thymeleaf"})
    public String index() {
        return "thymeleaf/index";
    }

    @RequestMapping("/smalllist.thymeleaf")
    public String smallList(final Model model) {
    	final List<Device> baseList = deviceRepository.findAll();
        model.addAttribute("entries", baseList.iterator());
        return "thymeleaf/smalllist";
    }

    @RequestMapping("/biglist.thymeleaf")
    public String bigList(final Model model) {
    	final List<Device> baseList = deviceRepository.findAll();
    	final Iterator<Device> deviceEntries = new Iterator<Device>() {

            private static final int REPEATS = 300;

            private int repeatCount = 0;
            private Iterator<Device> currentIterator = null;

            @Override
            public boolean hasNext() {
                if (this.currentIterator != null && this.currentIterator.hasNext()) {
                    return true;
                }
                if (this.repeatCount < REPEATS) {
                    this.currentIterator = baseList.iterator();
                    this.repeatCount++;
                    return true;
                }
                return false;
            }

            @Override
            public Device next() {
                return this.currentIterator.next();
            }

        };
        model.addAttribute("dataSource", deviceEntries);

        return "thymeleaf/biglist";
    }
}
