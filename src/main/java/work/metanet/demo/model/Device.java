package work.metanet.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity(name="t_device")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Device {
    /**
     * 设备ID
     */
	@Id
	private String deviceId;
    /**
     * 设备名称
     */
	private String deviceName;
    /**
     * MAC地址
     */
	private String wiredMac;
    /**
     * 无线MAC地址
     */
	private String wirelessMac;
    /**
     * IMEI
     */
	private String imei;
    /**
     * IMEI2
     */
	private String imei2;
    /**
     * 蓝牙
     */
	private String bluetooth;
    /**
     * 序列号
     */
	private String serialNumber;
    /**
     * UUID
     */
	private String uuid;
    /**
     * 品牌ID
     */
	private String brandId;
    /**
     * 型号ID
     */
	private String modelId;
    /**
     * 固件信息
     */
	private String firmwareInfo;
    /**
     * 来源 0导入/1记录
     */
	private String source;
    /**
     * 启用状态 0禁用/1启用
     */
	private String enableStatus;
    /**
     * 备注
     */
	private String remark;
    /**
     * 创建时间
     */
	private String createTime;
    /**
     * 修改时间
     */
	private String updateTime;
    /**
     * 创建用户
     */
	private String createUser;
    /**
     * 修改用户
     */
	private String updateUser;
    /**
     * 状态 0无效/1有效
     */
	private String status;
    /**
     * 真实标签 0虚拟/1真实
     */
	private String tag;
}
