package work.metanet.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafJpaMysqlBiglistApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafJpaMysqlBiglistApplication.class, args);
	}

}
