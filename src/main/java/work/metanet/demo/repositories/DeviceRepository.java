package work.metanet.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import work.metanet.demo.model.Device;

public interface DeviceRepository extends JpaRepository<Device, String> {
	public List<Device> findAll();
}
