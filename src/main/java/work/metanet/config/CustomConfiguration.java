package work.metanet.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "custom.config")
@Setter
@Getter
public class CustomConfiguration {
    /**
     * 大小
     */
    private int size;

    /**
     * 最大大小
     */
    private int maxSize;

    /**
     * 是否等待超时
     */
    private boolean awaitTimeout = false;

    /**
     * 前缀
     */
    private String prefix;

    /**
     * host配置
     */
    private Host host;

    @Getter
    @Setter
    public static class Host {

        /**
         * ip地址
         */
        private String ip;

        /**
         * 端口
         */
        private int port;
    }
}
