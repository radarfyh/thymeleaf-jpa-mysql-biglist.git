# 0. 介绍

## 0.1 功能

本项目功能是利用mysql数据库存储数据，并使用Spring JPA连接数据库;然后使用Thymeleaf展示表格。

## 0.2 准备工作

本项目采用mysql，初始化使用根目录自带的t_device.sql，里面包含表销毁、创建、记录插入等语句。
注意执行脚本之前要连接Mysql服务器，并执行：
create database th_test;
use th_test;
启动项目之前，要修改mysql的用户名和密码。

# 1. Thymeleaf 语法规则

在使用 Thymeleaf 之前，首先要在页面的 html 标签中声明名称空间，示例代码如下。
    
    xmlns:th="http://www.thymeleaf.org"

在 html 标签中声明此名称空间，可避免编辑器出现 html 验证错误，但这一步并非必须进行的，即使我们不声明该命名空间，也不影响 Thymeleaf 的使用。

Thymeleaf 作为一种模板引擎，它拥有自己的语法规则。Thymeleaf 语法分为以下 2 类：

* 标准表达式语法
* th 属性

## 1.1 标准表达式语法

Thymeleaf 模板引擎支持多种表达式：

1. 变量表达式：${...}
2. 选择变量表达式：*{...}
3. 链接表达式：@{...}
4. 国际化表达式：#{...}
5. 片段引用表达式：~{...}

### 1.1.1 变量表达式

使用 ${} 包裹的表达式被称为变量表达式，该表达式具有以下功能：

* 获取对象的属性和方法
* 使用内置的基本对象
* 使用内置的工具对象

#### 1.1.1.1获取对象的属性和方法

使用变量表达式可以获取对象的属性和方法，例如，获取 person 对象的 lastName 属性，表达式形式如下：

    ${person.lastName}

#### 1.1.1.2基本对象

使用变量表达式还可以使用内置基本对象，获取内置对象的属性，调用内置对象的方法。 Thymeleaf 中常用的内置基本对象如下：

* #ctx ：上下文对象；
* #vars ：上下文变量；
* #locale：上下文的语言环境；
* #request：HttpServletRequest 对象（仅在 Web 应用中可用）；
* #response：HttpServletResponse 对象（仅在 Web 应用中可用）；
* #session：HttpSession 对象（仅在 Web 应用中可用）；
* #servletContext：ServletContext 对象（仅在 Web 应用中可用）。

例如，我们通过以下 2 种形式，都可以获取到 session 对象中的 map 属性：

    ${#session.getAttribute('map')}
    ${session.map}

#### 1.1.1.3内置工具

除了能使用内置的基本对象外，变量表达式还可以使用一些内置的工具对象。

* strings：字符串工具对象，常用方法有：equals、equalsIgnoreCase、length、trim、toUpperCase、toLowerCase、indexOf、substring、replace、startsWith、endsWith，contains 和 containsIgnoreCase 等；
* numbers：数字工具对象，常用的方法有：formatDecimal 等；
* bools：布尔工具对象，常用的方法有：isTrue 和 isFalse 等；
* arrays：数组工具对象，常用的方法有：toArray、length、isEmpty、contains 和 containsAll 等；
* lists/sets：List/Set 集合工具对象，常用的方法有：toList、size、isEmpty、contains、containsAll 和 sort 等；
* maps：Map 集合工具对象，常用的方法有：size、isEmpty、containsKey 和 containsValue 等；
* dates：日期工具对象，常用的方法有：format、year、month、hour 和 createNow 等。

例如，我们可以使用内置工具对象 strings 的 equals 方法，来判断字符串与对象的某个属性是否相等，代码如下。

    ${#strings.equals('程序员',name)}

### 1.1.2 选择变量表达式

选择变量表达式与变量表达式功能基本一致，只是在变量表达式的基础上增加了与 th:object 的配合使用。当使用 th:object 存储一个对象后，我们可以在其后代中使用选择变量表达式（*{...}）获取该对象中的属性，其中，“*”即代表该对象。

    <div th:object="${session.user}" >
        <p th:text="*{fisrtName}">firstname</p>
    </div>

th:object 用于存储一个临时变量，该变量只在该标签及其后代中有效，在后面的内容“th 属性”中我详细介绍。

### 1.1.3 链接表达式

不管是静态资源的引用，还是 form 表单的请求，凡是链接都可以用链接表达式 （@{...}）。

链接表达式的形式结构如下：

* 无参请求：@{/xxx}
* 有参请求：@{/xxx(k1=v1,k2=v2)}

例如使用链接表达式引入 css 样式表，代码如下。

    <link href="asserts/css/signin.css" th:href="@{/asserts/css/signin.css}" rel="stylesheet">

### 1.1.4 国际化表达式
消息表达式一般用于国际化的场景。结构如下。

    th:text="#{msg}"

注意：此处了解即可，我们会在后面的章节中详细介绍。

### 1.1.5 片段引用表达式

片段引用表达式用于在模板页面中引用其他的模板片段，该表达式支持以下 2  中语法结构：

* 推荐：~{templatename::fragmentname}
* 支持：~{templatename::#id}

以上语法结构说明如下：

* templatename：模版名，Thymeleaf 会根据模版名解析完整路径：/resources/templates/templatename.html，要注意文件的路径。
* fragmentname：片段名，Thymeleaf 通过 th:fragment 声明定义代码块，即：th:fragment="fragmentname"
* id：HTML 的 id 选择器，使用时要在前面加上 # 号，不支持 class 选择器。

## 1.2 th 属性
Thymeleaf 还提供了大量的 th 属性，这些属性可以直接在 HTML 标签中使用，其中常用 th 属性及其示例如下表。

![](src/main/resources/static/images/TH属性介绍1.png)
![](src/main/resources/static/images/TH属性介绍2.png)
![](src/main/resources/static/images/TH属性介绍3.png)​

# 2. Thymeleaf 公共页面抽取
Thymeleaf 作为一种优雅且高度可维护的模板引擎，同样支持公共页面的抽取和引用。我们可以将公共页面片段抽取出来，存放到一个独立的页面中，并使用 Thymeleaf 提供的 th:fragment 属性为这些抽取出来的公共页面片段命名。

将公共页面片段抽取出来，存放在 commons.html 中，代码如下。

    <div th:fragment="fragment-name" id="fragment-id">
        <span>公共页面片段</span>
    </div>

## 2.1 引用公共页面

在 Thymeleaf 中，我们可以使用以下 3 个属性，将公共页面片段引入到当前页面中。
* th:insert：将代码块片段整个插入到使用了 th:insert 属性的 HTML 标签中；
* th:replace：将代码块片段整个替换使用了 th:replace 属性的 HTML 标签中；
* th:include：将代码块片段包含的内容插入到使用了 th:include 属性的 HTML 标签中。

使用上 3 个属性引入页面片段，都可以通过以下 2 种方式实现。
* ~{templatename::selector}：模板名::选择器
* ~{templatename::fragmentname}：模板名::片段名

    通常情况下，~{} 可以省略，其行内写法为 [[~{...}]] 或 [(~{...})]，其中  [[~{...}]] 会转义特殊字符，[(~{...})] 则不会转义特殊字符。

** (1)在页面 fragment.html 中引入 commons.html 中声明的页面片段，可以通过以下方式实现： **
    
    <!--th:insert 片段名引入-->
    <div th:insert="commons::fragment-name"></div>
    <!--th:insert id 选择器引入-->
    <div th:insert="commons::#fragment-id"></div>
***
    <!--th:replace 片段名引入-->
    <div th:replace="commons::fragment-name"></div>
    <!--th:replace id 选择器引入-->
    <div th:replace="commons::#fragment-id"></div>
***
    <!--th:include 片段名引入-->
    <div th:include="commons::fragment-name"></div>
    <!--th:include id 选择器引入-->
    <div th:include="commons::#fragment-id"></div>

** (2)启动 Spring Boot，使用浏览器访问 fragment.html，查看源码，结果如下： **

    <!--th:insert 片段名引入-->
    <div>
        <div id="fragment-id">
            <span>公共页面片段</span>
        </div>
    </div>
    <!--th:insert id 选择器引入-->
    <div>
        <div id="fragment-id">
            <span>公共页面片段</span>
        </div>
    </div>
*** 
    <!--th:replace 片段名引入-->
    <div id="fragment-id">
        <span>公共页面片段</span>
    </div>
    <!--th:replace id 选择器引入-->
    <div id="fragment-id">
        <span>公共页面片段</span>
    </div>

***

    <!--th:include 片段名引入-->
    <div>
        <span>公共页面片段</span>
    </div>
    <!--th:include id 选择器引入-->
    <div>
        <span>公共页面片段</span>
    </div>

## 2.2 传递参数

Thymeleaf 在抽取和引入公共页面片段时，还可以进行参数传递，大致步骤如下：
* 传入参数；
* 使用参数。

### 2.2.1 传入参数
引用公共页面片段时，我们可以通过以下 2 种方式，将参数传入到被引用的页面片段中：

* 模板名::选择器名或片段名(参数1=参数值1,参数2=参数值2)
* 模板名::选择器名或片段名(参数值1,参数值2)

注：
若传入参数较少时，一般采用第二种方式，直接将参数值传入页面片段中；
若参数较多时，建议使用第一种方式，明确指定参数名和参数值，。


    <!--th:insert 片段名引入-->
    <div th:insert="commons::fragment-name(var1='insert-name',var2='insert-name2')"></div>
    <!--th:insert id 选择器引入-->
    <div th:insert="commons::#fragment-id(var1='insert-id',var2='insert-id2')"></div>
***
    <!--th:replace 片段名引入-->
    <div th:replace="commons::fragment-name(var1='replace-name',var2='replace-name2')"></div>
    <!--th:replace id 选择器引入-->
    <div th:replace="commons::#fragment-id(var1='replace-id',var2='replace-id2')"></div>

***
    <!--th:include 片段名引入-->
    <div th:include="commons::fragment-name(var1='include-name',var2='include-name2')"></div>
    <!--th:include id 选择器引入-->
    <div th:include="commons::#fragment-id(var1='include-id',var2='include-id2')"></div>

### 2.2.2 使用参数

在声明页面片段时，我们可以在片段中声明并使用这些参数，例如：

    <!--使用 var1 和 var2 声明传入的参数，并在该片段中直接使用这些参数 -->
    <div th:fragment="fragment-name(var1,var2)" id="fragment-id">
        <p th:text="'参数1:'+${var1} + '-------------------参数2:' + ${var2}">...</p>
    </div>
